//
//  ViewController.swift
//  DemoRealm
//
//  Created by LeAnhDuc on 26/04/2022.
//

import RealmSwift
import UIKit

class ViewController: UIViewController {
    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var priceTextField: UITextField!
    @IBOutlet var amountTextField: UITextField!
    @IBOutlet var foodListTableView: UITableView!

    var foodNotificationToken: NotificationToken?

    var foods = [Food]()
    let realm = try! Realm()
    var selectedFood: Food?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        // fetch list food
        let result = realm.objects(Food.self)
        foodNotificationToken = result.observe { _ in
            self.foodListTableView.reloadData()
            self.selectedFood = nil
        }
        foods = result.reversed()
    }

    @IBAction func addAction(_ sender: Any) {
        if nameTextField.text != "" && priceTextField.text != "" && amountTextField.text != "" {
            let queryFood = realm.objects(Food.self).where {
                $0.name == selectedFood?.name ?? "" && $0.price == selectedFood?.price ?? 0 && $0.amount == selectedFood?.amount ?? 0
            }

            if queryFood.reversed().isEmpty {
                try! realm.write {
                    let food = Food()
                    food.name = nameTextField.text!
                    food.price = Int(priceTextField.text!) ?? 0
                    food.amount = Int(amountTextField.text!) ?? 0
                    foods.append(food)
                    realm.add(foods)
                }
            } else {
                try! realm.write {
                    queryFood.first?.name = nameTextField.text!
                    queryFood.first?.price = Int(priceTextField.text!) ?? 0
                    queryFood.first?.amount = Int(amountTextField.text!) ?? 0
                }
            }
            
            nameTextField.text?.removeAll()
            priceTextField.text?.removeAll()
            amountTextField.text?.removeAll()
        }
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return foods.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? FoodListTableViewCell else { return UITableViewCell() }

        cell.nameLabel.text = foods[indexPath.row].name
        cell.priceLabel.text = "\(foods[indexPath.row].price)"
        cell.amountLabel.text = "\(foods[indexPath.row].amount)"

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        nameTextField.text = foods[indexPath.row].name
        priceTextField.text = "\(foods[indexPath.row].price)"
        amountTextField.text = "\(foods[indexPath.row].amount)"
        selectedFood = foods[indexPath.row]
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            try! realm.write{
                realm.delete(foods.remove(at: indexPath.row))
            }
        }else if editingStyle == .insert{
            
        }
    }
}
