//
//  Food.swift
//  DemoRealm
//
//  Created by LeAnhDuc on 26/04/2022.
//

import Foundation
import RealmSwift

class Food: Object{
    @objc dynamic var name: String = ""
    @objc dynamic var price: Int = 0
    @objc dynamic var amount: Int = 0
}


